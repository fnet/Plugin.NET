﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterfaceLib
{
    /// <summary>
    /// 接口
    /// </summary>
    public interface IInterface
    {
        string Method1();

        string Method2(string from);
    }
}
