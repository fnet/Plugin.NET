﻿using InterfaceLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PluginLib1
{
    public class PluginClass1 : IInterface
    {
        public string Method1()
        {
            return "这是从第一个插件里面来的数据";
        }

        public string Method2(string from)
        {
            return "这是从第一个插件里面来的数据:"+from;
        }
    }
}
