﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PluginNET.error
{

    /// <summary>
    /// 在实例化 PluginManager 时，泛型参数不是接口时会抛出这个异常
    /// </summary>
    [Serializable]
    public class PluginInvalidGeneralTypeException : Exception
    {
        public PluginInvalidGeneralTypeException() : base("泛型参数不是接口")
        {

        }
    }
}
