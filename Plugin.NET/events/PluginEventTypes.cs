﻿namespace PluginNET.events
{
    /// <summary>
    /// 插件事件的类型枚举
    /// </summary>
    public enum PluginEventTypes
    {
        /// <summary>
        /// 加载程序集
        /// </summary>
        AssemblyLoading,

        /// <summary>
        /// 程序集加载后
        /// </summary>
        AssemblyLoaded,

        /// <summary>
        /// 查找可用的class类
        /// </summary>
        ClassSearching,

        /// <summary>
        /// 创建实例后
        /// </summary>
        InstanceCreated,
    }
}
